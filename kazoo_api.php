<?php namespace Kazoo;

	$kazoo_url = "kazooapi.ztpbx.ru/v1";
	define('account_id', 'e44e61decf894cb4e5e07309cfdc63eb');

	function get($account_id, $auth_token, $params) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/".$account_id."/".$params);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                
			'X-Auth-Token: '.$auth_token
		));
		$output = curl_exec($ch);
		$res = json_decode($output, false);

		curl_close($ch);
		
		return $res;
	}
	
	function post($account_id, $auth_token, $url, $body) {
		global $kazoo_url;
		$ch = curl_init();
				
		$data_string = json_encode($body);
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/".$account_id."/".$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string),
			'X-Auth-Token: '.$auth_token
		));

		$output = curl_exec($ch);
		$output = json_decode($output, false);
		curl_close($ch);
		
		return $output;
	}
	
	function delete ($account_id, $auth_token, $url) {
		global $kazoo_url;

	    $ch = curl_init();
	    
	    curl_setopt($ch, CURLOPT_URL,$kazoo_url."/accounts/$account_id/$url");
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	    
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'X-Auth-Token: '.$auth_token
		));
	    
	    $output = curl_exec($ch);
		$output = json_decode($output, false);
		curl_close($ch);

	    return $output;
	}
	
	function put($account_id, $auth_token, $url, $body) {
		global $kazoo_url;
		$ch = curl_init();

        $data_string = json_encode($body);
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/".$account_id."/".$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string),
			'X-Auth-Token: '.$auth_token
		));

		$output = curl_exec($ch);
        $output = json_decode($output, false);
		curl_close($ch);
		
		return $output;
	}
	
	function deleteGroup($account_id, $auth_token, $group_id)	{
		global $kazoo_url;

	    $ch = curl_init();
	    
	    curl_setopt($ch, CURLOPT_URL,$kazoo_url."/accounts/$account_id/groups/$group_id");
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	    
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'X-Auth-Token: '.$auth_token
		));
	    
	    $output = curl_exec($ch);
		$output = array("output" => json_decode($output, false), "response_code" => curl_getinfo($ch, CURLINFO_HTTP_CODE));
	    curl_close($ch);

	    return $output;
	}
	
	function deletePhoneNumber($account_id, $auth_token, $phone_number)	{
		global $kazoo_url;

	    $ch = curl_init();
	    
	    curl_setopt($ch, CURLOPT_URL,$kazoo_url."/accounts/$account_id/phone_numbers/$phone_number");
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'X-Auth-Token: '.$auth_token
		));
	    
	    $output = curl_exec($ch);
		$output = json_decode($output, false);
		curl_close($ch);

	    return $output;
	}
	
	function deleteCallflow($account_id, $auth_token, $callflow_id)	{
		global $kazoo_url;

	    $ch = curl_init();
	    
	    curl_setopt($ch, CURLOPT_URL,$kazoo_url."/accounts/$account_id/callflows/$callflow_id");
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
	    
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'X-Auth-Token: '.$auth_token
		));
	    
	    $output = curl_exec($ch);
		$output = array("output" => json_decode($output, false), "response_code" => curl_getinfo($ch, CURLINFO_HTTP_CODE));
	    curl_close($ch);

	    return $output;
	}

	function getDevices($account_id, $extension, $auth_token) {
		$callflow_id = getCallflowID($account_id, $auth_token, $extension->extension);
		$callflow = getCallflow($callflow_id, $account_id, $auth_token);
		$devices = getEndpointsFromCallflow($callflow, $extension->device_type);

		return $devices;
	}

	function getTokenApiZebra() {
		$login = 'admin'; $password = 'Pt,hjdcrbqU0k0c';
		$update = array(
				"data" => array(
				"login" => $login,
				"password" => $password,
				"realm" => "zebratelecom.ztpbx.ru"
			));

		$data_string = json_encode($update);
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, "api.zebratelecom.ru/v1/kazoos/user_auth");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string)
		));

		$output = curl_exec($ch);
		$auth_token = json_decode($output, true);
		curl_close($ch);
		
		return $auth_token['auth_token'];
	}
	
	// Авторизация
	function getToken() {
		global $kazoo_url;

		$update = array(
				"data" => array(
				"credentials" => "0d9cfe9462f0c0d9b2c543ef7d68721e",
				"account_name" => "superadmin"
			));

		$data_string = json_encode($update);
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/user_auth");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string)
		));

		$output = curl_exec($ch);
		$auth_token = json_decode($output, true);
		curl_close($ch);
		
		return $auth_token['auth_token'];
	}
	
	function getDomains($auth_token) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/e44e61decf894cb4e5e07309cfdc63eb/children");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);
		$res = json_decode($output, false);

		curl_close($ch);
		return $res;
	}
	
	function getDomain($domain_id, $auth_token) {
		global $kazoo_url;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$domain_id");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);
		$output = json_decode($output);
		
		curl_close($ch);
		
		return $output;
	}
	
	function getDomainID($auth_token, $domain) {
		global $kazoo_url;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/sup/crossbar/find_account_by_realm/".$domain);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		
		$output = curl_exec($ch);
		$output = json_decode($output);
		$output = $output->data;
		if (gettype($output) === 'string') {
			$output = substr($output, 7, 32);
		} else {
			$output = '';
		}
		
		curl_close($ch);
		
		return $output;
	}
	
	function getDomainIDByNumber($auth_token, $number) {
		global $kazoo_url;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/sup/crossbar/find_account_by_number/".$number);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		
		$output = curl_exec($ch);
		$output = json_decode($output);
		$output = $output->data;
		if (gettype($output) === 'string') {
			$output = substr($output, 7, 32);
		} else {
			$output = '';
		}
		
		curl_close($ch);
		
		return $output;
	}
	
	/*
		Получение phone_numbers
		Возвращает json
	*/
	function getPhoneNumbers($domain_id, $auth_token) {
		global $kazoo_url;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$domain_id/phone_numbers");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);
		$output = json_decode($output);

		curl_close($ch);
		
		return $output;
	}
	
	/*
		Получение registrations
		Возвращает json
	*/
	function getRegistrations($domain_id, $auth_token) {
		global $kazoo_url;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$domain_id/registrations");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);
		$output = json_decode($output);

		curl_close($ch);
		
		return $output;
	}
		

	function getCallflowID($account_id, $auth_token, $add_number) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/".$account_id."/callflows?filter_numbers=".$add_number);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);
		$res = json_decode($output, false);

		curl_close($ch);
		
		return $res->data[0]->id;
	}
	
	function getCallflow($callflow_id, $account_id, $auth_token) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/".$account_id."/callflows/".$callflow_id);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);

		curl_close($ch);
		
		return json_decode($output, false);
	}
	
	function getMetaflows($account_id, $auth_token) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/".$account_id."/metaflows");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);
		curl_close($ch);
		return json_decode($output, false);
	}
	
	function getCallflowIDByOwner($account_id, $auth_token, $owner_id) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/".$account_id."/callflows?filter_owner_id=".$owner_id);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);
		$res = json_decode($output, false);

		curl_close($ch);
		
		return $res->data[0]->id;
	}
	
	function getGroup($account_id, $auth_token, $group_id) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/groups/$group_id");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                   
			'Content-Type: application/json',                                                                         
			'X-Auth-Token: '.$auth_token
		));

		$output = curl_exec($ch);

		curl_close($ch);
		
		return json_decode($output, false);
	}
	
	function getDevice($account_id, $device_id, $auth_token) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/devices/$device_id");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                   
			'Content-Type: application/json',                                                                         
			'X-Auth-Token: '.$auth_token
		));

		$output = curl_exec($ch);

		curl_close($ch);
		
		return json_decode($output, false);
	}
	
	function getMedia($account_id, $filename, $auth_token) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/media?filter_name=".urlencode($filename));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                   
			'Content-Type: application/json',
			'Accept: application/json',                                                                       
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);
		curl_close($ch);
		
		return json_decode($output, false);
	}
	
	function getVMBoxID($account_id, $extension, $auth_token) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/vmboxes?filter_mailbox=$extension");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                   
			'Content-Type: application/json',                                                                        
			'X-Auth-Token: '.$auth_token
		));

		$output = curl_exec($ch);

		curl_close($ch);
		
		return json_decode($output, false);
	}
	
	function getVMBoxes($account_id, $auth_token) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/vmboxes");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                   
			'Content-Type: application/json',                                                                        
			'X-Auth-Token: '.$auth_token
		));

		$output = curl_exec($ch);

		curl_close($ch);
		
		return json_decode($output, false);
	}
	
	function postVMBox($account_id, $vmbox_id, $data, $auth_token) {
		global $kazoo_url;

		$data_string = json_encode($data); 

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/vmboxes/$vmbox_id");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',
			'X-Auth-Token: '.$auth_token,
			'Content-Length: ' . strlen($data_string)                                                                      
		));       

		$output = curl_exec($ch);

		curl_close($ch);
		
		return $output;
	}
	
	function postMetaflows($account_id, $auth_token, $data) {
		global $kazoo_url;

		//$data_string = json_encode($data); 
		$data_string = $data; 

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/metaflows");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',
			'X-Auth-Token: '.$auth_token,
			'Content-Length: ' . strlen($data_string)                                                                      
		));       

		$output = curl_exec($ch);

		curl_close($ch);
		
		return $output;
	}
	
	function putContact($account_id, $auth_token, $list_id, $data) {
		global $kazoo_url;

		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/lists/$list_id");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data),
			'X-Auth-Token: '.$auth_token
		));

		$output = curl_exec($ch);
		$output = array("output" => json_decode($output, false), "response_code" => curl_getinfo($ch, CURLINFO_HTTP_CODE));
		curl_close($ch);
		
		return $output;
	}
	
	function getVMBox($account_id, $vmbox_id, $auth_token) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/vmboxes/$vmbox_id");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                   
			'Content-Type: application/json',                                                                        
			'X-Auth-Token: '.$auth_token
		));

		$output = curl_exec($ch);

		curl_close($ch);
		
		return json_decode($output, false);
	}
	
	function getUserID($account_id, $extension, $auth_token) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/users?filter_caller_id.internal.number=$extension");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',                                                                        
			'X-Auth-Token: '.$auth_token
		));

		$output = curl_exec($ch);

		curl_close($ch);
		
		return json_decode($output, false);
	}
	
	/*
		Получение user
		Возвращает json
	*/
	function getUser($domain_id, $user_id, $auth_token) {
		global $kazoo_url;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$domain_id/users/$user_id");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',                                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);
		$output = json_decode($output);

		curl_close($ch);
		
		return $output;
	}
	
		/*
		Получение user (admin)
		Возвращает json
	*/
	function getAdmin($domain_id, $auth_token) {
		global $kazoo_url;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$domain_id/users?filter_priv_level=admin");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',                                                                                
			'X-Auth-Token: '.$auth_token
		));
		
		$output = curl_exec($ch);
		$output = json_decode($output, false);

		curl_close($ch);
		
		return $output;
	}
	
	function getConferenceID($account_id, $conf_name, $auth_token) {
		global $kazoo_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $kazoo_url."/accounts/$account_id/conferences?filter_name=$conf_name");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',                                                                        
			'X-Auth-Token: '.$auth_token
		));

		$output = curl_exec($ch);

		curl_close($ch);
		
		return json_decode($output, false);
	}