<?php

require_once '../kazoo_api.php';
require_once 'json_find.php';
require_once 'log.php';
Log::write("Fixing extensions started", Log::$L_INFO);

$account_id = $_GET['account_id'];
$auth_token = $_GET['auth_token'];
$extension = $_GET['extension'];

// Checking for input params
if (strlen($account_id) !== 32 || strlen($auth_token) !== 32 || !isset($extension)) {
	Log::write('{"text": "Invalid input params", "success": false}', Log::$L_INFO);
	echo '{"text": "Invalid input params", "success": false}';
	return;
}

// Get callflow using extension filter
$callflow = Kazoo\get ($account_id, $auth_token, 'callflows?filter_numbers='.$extension);
if ($callflow->status === 'error') {
	Log::write('{"text": "'.$callflow->data.'", "success": false}', Log::$L_INFO);
	echo '{"text": "'.$callflow->data.'", "success": false}';
	return;
}

$callflow = $callflow->data[0];

if (!isset($callflow)) {
	Log::write('{"text": "Добавочный '.$extension.' не найден в домене<br>", "success": true}', Log::$L_INFO);
	echo '{"text": "Добавочный '.$extension.' не найден в домене<br>", "success": true}';
	return;
}

if (isset($callflow->owner_id)) {
	if (strlen($callflow->owner_id) === 32) {
		$user = Kazoo\get ($account_id, $auth_token, 'users/'.$callflow->owner_id);
		// if entity was found
		if ($user->status === 'success' && $user->status !== 'error') {
			Log::write('{"text": "Добавочный занят - '.$user->data->first_name.'. Все корректно.", "success": true}', Log::$L_INFO);
			echo '{"text": "Добавочный занят - '.$user->data->first_name.'. Все корректно.", "success": true}';
			return;
		}
	}
}

if(isset($callflow->name)) {
	if (substr($callflow->name, 0, 4) !== 'user') {
		Log::write('{"text": "Добавочный принадлежит не пользователю. Он занят за '.$callflow->name.'", "success": true}', Log::$L_INFO);
		echo '{"text": "Добавочный принадлежит не пользователю. Он занят за '.$callflow->name.'", "success": true}';
		return;
	}
}


$output = '';

// Delete callflow
if (isset($callflow->id)) {
	if (strlen($callflow->id) === 32) {
		$delCallflow = Kazoo\delete ($account_id, $auth_token, 'callflows/'.$callflow->id);
		if ($delCallflow->status === 'success') {
			$output .= 'Сценарий '.$delCallflow->data->name.' удален<br>';
		}
	}
}


// Delete devices
$ring_group = find ($delCallflow, $delCallflow, 'module', 'ring_group');
if (isset($ring_group->data->endpoints)) {
	foreach ($ring_group->data->endpoints as $endpoint) {
		if (strlen($endpoint->id) !== 32) continue;
		$delDevice = Kazoo\delete ($account_id, $auth_token, 'devices/'.$endpoint->id);
		if ($delDevice->status === 'success') {
			$output .=  'Устройство '.$delDevice->data->name.' удалено<br>';
		}
	}
}

// Delete vmbox
$voicemail = find ($delCallflow, $delCallflow, 'module', 'voicemail');
if (strlen($voicemail->data->id) === 32) {
	$delVoiceMail = Kazoo\delete ($account_id, $auth_token, 'vmboxes/'.$voicemail->data->id);
	if ($delVoiceMail->status === 'success') {
		$output .=  'Ящик ГП '.$delVoiceMail->data->name.' удален<br>';
	}
}

echo '{"text": "'.$output.'", "success": true}';

Log::write("Fixing extensions ended: ".json_encode($output), Log::$L_INFO);