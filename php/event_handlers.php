<?php

function generalEvent () {

	$filename_general = '/var/www/html/htdocs/kazoosupport/logs/webhook_general.txt';
	file_put_contents($filename_general, json_encode($_GET)."\n\n\n\n\n\n\n", FILE_APPEND);
	
	$auth_token = getAuthToken();
	if (!$auth_token) return;
	
	$admin = Kazoo\get ($_GET['account_id'], $auth_token, 'users?filter_priv_level=admin')->data[0];
	
	if (strlen($admin->id) !== 32) {
		log_msg('Invalid admin id '.json_encode($admin));
		return;
	}
	$adminData = Kazoo\get ($_GET['account_id'], $auth_token, 'users/'.$admin->id);
	if ($adminData->data->general_missed_calls_notify !== true) {
		log_msg('User(admin) '.$adminData->data->id.'. general_missed_calls_notify is disable');
		return;
	}
	
	sendMailMessage($adminData);
	
}

function personalEvent () {

	$filename = '/var/www/html/htdocs/kazoosupport/logs/webhook_personal.txt';
	file_put_contents($filename, json_encode($_GET)."\n\n\n\n\n\n\n", FILE_APPEND);

	// if account_id or owner id are incorrect return function
	if (strlen($_GET['account_id']) !== 32 || strlen($_GET['owner_id']) !== 32) {
		log_msg('account_id or owner_id in event JSON are incorrect');
		return;
	}

	// Read call_id from missed_calls_call_id.txt
	$filename_messed_calls_id = "/var/www/html/htdocs/kazoosupport/logs/missed_calls_call_id.txt";
	if (file_exists($filename_messed_calls_id)) {
		$fromFile = file_get_contents($filename_messed_calls_id);
		if($fromFile) {
			$json = json_decode($fromFile, false);
			if (callIdExistInFile($json)) {
				log_msg('Call id '.$json->call_id.' exist in file');
				return;
			}
		}
	}
	

	$auth_token = getAuthToken();
	if (!$auth_token) return;

	$user = Kazoo\get ($_GET['account_id'], $auth_token, 'users/'.$_GET['owner_id']);
	if (!userWantToGetNotify($user, $auth_token)) {
		log_msg('User '.$user->data->id.' missed_calls_notify is disable');
		return;
	}

	sendMailMessage ($user);

	if (!isset($json->call_id)) {
		$json = (object) array();
		$json->call_id = array();
	}

	$json->call_id[] = $_GET['call_id'];
	log_msg('Call_id '.$_GET['call_id'].' was added to file');
	file_put_contents("/var/www/html/htdocs/kazoosupport/logs/missed_calls_call_id.txt", json_encode($json));
}