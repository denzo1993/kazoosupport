<?php

require_once '../kazoo_api.php';
require_once 'log.php';

$account_id = $_GET['account_id'];
$auth_token = $_GET['auth_token'];

$data = '{"data":{"pattern":"autocall","firstname":"true"}}';

$registry_id = Kazoo\get($account_id, $auth_token, 'lists?filter_name=registry')->data[0]->id;
$addAutoCallRes = Kazoo\putContact($account_id, $auth_token, $registry_id, $data);

echo json_encode($addAutoCallRes);

Log::write("Enable autocall: ".json_encode($addAutoCallRes), Log::$L_INFO);