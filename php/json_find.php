<?php

function find ($json, $prev_node, $object, $object_val) {
	foreach ($json as $key => $val) {
		if (gettype($val) == "object") {
			$findRes = find ($val, $val, $object, $object_val);
			if($findRes !== null) {
				return $findRes;
			}
		} else {
			if ($key == $object) {
				if ($val == $object_val) {
					return $prev_node;
				}
			}
		}
	}
}