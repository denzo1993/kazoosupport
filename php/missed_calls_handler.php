<?php

require_once './missed_calls_functions.php';
require_once './event_handlers.php';
require_once '../kazoo_api.php';
require_once './Mail-1.3.0/Mail.php';

log_msg(' ################# New channel destroy event ################# ');

$filename_full = '/var/www/html/htdocs/kazoosupport/logs/webhook_full.txt';
file_put_contents($filename_full, json_encode($_GET)."\n\n\n\n\n\n\n", FILE_APPEND);

// General notify
if (webHookGeneralFilter()) {
	log_msg('It is general event');
	generalEvent();
	return;
}

// Personal notify
if (webHookPersonalNotifyFilter()) {
	log_msg('It is personal event');
	personalEvent();
	return;
}