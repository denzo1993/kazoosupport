﻿<?php

require_once '../kazoo_api.php';
require_once 'log.php';
Log::write("Adding contacts", Log::$L_INFO);

$account_id = $_POST['account_id'];
$auth_token = $_POST['auth_token'];

$json = file_get_contents($_FILES['contacts']['tmp_name']);
$json = json_decode($json, false);

// Получение id address_book
$address_book_id = Kazoo\get($account_id, $auth_token, 'lists?filter_name=address_book')->data[0]->id;
if(!isset($address_book_id)) { echo "Can't find address_book id"; return; }



foreach ($json->data as $contact) {
	//if(!check(array("pattern" => $contact->pattern, "firstname" => $contact->firstname, "lastname" => $contact->lastname, "type" => $contact->type))) continue;
	$data = array(
		"data" => array(
			"pattern" => "^\+?".$contact->pattern."\d*",
			"firstname" => $contact->firstname,
			"lastname" => $contact->lastname,
			"type" => $contact->type
		)
	);
	
	$putContactRes = Kazoo\putContact($account_id, $auth_token, $address_book_id, normJsonStr(json_encode($data)));
	
	if($putContactRes['response_code'] == 201) {
		echo $contact->pattern." добавлен</br>";
	} else {
		echo $contact->pattern." НЕ добавлен</br>";
	}
}

echo "Конец операции!";
Log::write("Adding contacts have finished. address_book id: ".$address_book_id, Log::$L_INFO);

/*
function check($params) {
	foreach($params as $param) {
		//if(!isset($param)) return false;
	//}
	if(!preg_match("/[^0-9]/", $params['pattern'], $matches)) return false;
}
*/

function normJsonStr($str){
    $str = preg_replace_callback('/\\\u([a-f0-9]{4})/i', create_function('$m', 'return chr(hexdec($m[1])-1072+224);'), $str);
    return iconv('cp1251', 'utf-8', $str);
}