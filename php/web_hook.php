<?php

function checkWebHook ($account_id, $auth_token) {
	
	$webhook = Kazoo\get ($account_id, $auth_token, 'webhooks?filter_hook=channel_destroy');
	
	$output = (object) array();
	$output->enabled = false;
	
	if (isset($webhook->data[0]->hook)) {
		$output->webhook_id = $webhook->data[0]->id;
		if ($webhook->data[0]->hook === 'channel_destroy') {
			if ($webhook->data[0]->enabled === true) {
				if ($webhook->data[0]->uri === 'http://kazoosupport.ztel.ru/support/php/missed_calls_handler.php') {
					$output->enabled = true;
				}
			}
		}
	}
	
	return $output;
}