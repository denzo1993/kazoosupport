<?php

require_once '../kazoo_api.php';
require_once 'log.php';

$account_id = $_GET['account_id'];
$vmbox_id = $_GET['vmbox_id'];
$value = $_GET['value'];
$auth_token = $_GET['auth_token'];
if($value == '0') $value = false; else $value = true;

$vmbox = Kazoo\getVMBox($account_id, $vmbox_id, $auth_token);
$vmbox->data->skip_instructions = $value;

$postRes = Kazoo\postVMBox($account_id, $vmbox_id, $vmbox, $auth_token);

Log::write("Change vmbox greeting state: ".json_encode($postRes), Log::$L_INFO);

echo $postRes;