<?php

require_once('/var/www/html/htdocs/kazoosupport/public/support/kazoo_api.php');

// if can't get auth token, system will try to get it for 3 hours with step 5 mins
for ($i = 0; $i<36; $i++) {
	$auth_token = Kazoo\getTokenApiZebra();
	if (strlen($auth_token) === 32) {
		break;
	}
	sleep(5 * 60);
}

file_put_contents('/var/www/html/htdocs/kazoosupport/logs/tzt.txt', $auth_token);
