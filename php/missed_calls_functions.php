<?php

function webHookGeneralFilter () {
	// Only inbound
	if (!isset($_GET['call_direction'])) return false;
	if ($_GET['call_direction'] !== 'inbound') return false;
	if (!strpos($_GET['request'], '@213.145.53.135')) return false;
	if (isset($_GET['other_leg_call_id'])) return false;
	return true; 
}

function webHookPersonalNotifyFilter () {
	// if there isn't channel_destroy return function
	if (!isset($_GET['hook_event'])) return false;
	if ($_GET['hook_event'] !== 'channel_destroy') return false;

	// Only outbound
	if (!isset($_GET['call_direction'])) return false;
	if ($_GET['call_direction'] === 'inbound') return false;

	// If NORMAL_CLEARING return function
	if (isset($_GET['hangup_cause'])) {
		if ($_GET['hangup_cause'] === 'NORMAL_CLEARING') return false;
	}

	if (!strpos($_GET['request'], 'ztpbx.ru')) return false;
	
	return true;
}

function callIdExistInFile ($json) {
	if (isset($json->call_id)) {
		foreach ($json->call_id as $call) {
			if ($call === $_GET['call_id']) {
				error_log('call_id '.$_GET['call_id'].' найден в массиве call_id');
				return true;
			}
		}
	}
	return false;
}

function userWantToGetNotify ($user, $auth_token) {
	if (!isset($user->data->missed_calls_notify)) return false;
	return ($user->data->missed_calls_notify === true);
}

function sendMailMessage ($user) {
	// Send email message
	$headers = array(
			'From' => 'debulanov@yandex.ru',
			'To' => $user->data->email,
			'Subject' => 'Пропущенный вызов от '.$_GET['caller_id_number'],
			'Content-Type' => 'text/html; charset=UTF-8'
		);

	$smtp = Mail::factory('smtp', array(
	    'host' => 'ssl://smtp.yandex.ru',
	    'port' => '465',
	    'auth' => true,
	    'username' => 'debulanov@yandex.ru',
	    'password' => '078754123'
	));

	$datetime = date('m/d/Y H:i:s', $_GET['timestamp']);
	$text = 'Пропущенный вызов от '.$_GET['caller_id_number'].' в '.$datetime;

	$mail = $smtp->send($user->data->email, $headers, $text);
	if (PEAR::isError($mail)) {
		log_msg($user->data->email.' Email error - '.$mail->getMessage());
		return false;
	} else {
		log_msg('Message was successfully sent to '.$user->data->email);
		return true;
	}
}

function log_msg($log) {
	$datetime = date('m/d/Y H:i:s', $_GET['timestamp']);
	file_put_contents("/var/www/html/htdocs/kazoosupport/logs/missed_calls.log", "[".$datetime." call_id: ".$_GET['call_id']."]   ".$log."\n", FILE_APPEND);
}

function getAuthToken () {
	$auth_token = file_get_contents('/var/www/html/htdocs/kazoosupport/logs/t.txt');
	if (strlen($auth_token) !== 32) {
		log_msg('Invalid auth_token '.$auth_token);
		return false;
	}
	return $auth_token;
}