<?php

require_once '../kazoo_api.php';
require_once 'log.php';
Log::write("First aid. Delete plus started", Log::$L_INFO);

$account_id = $_GET['account_id'];
$auth_token = $_GET['auth_token'];

$log_callflows = deletePlusFromCallflows($account_id, $auth_token);
$log_phone_numbers = deletePlusFromPhoneNumbers($account_id, $auth_token);

Log::write("First aid. Delete plus ended: ".json_encode($log_callflows)." ".json_encode($log_phone_numbers), Log::$L_INFO);

echo "<b><i>Результат поиска в сценариях:</i></b>"."<br>";
if ($log_callflows !== '') {
	echo $log_callflows;
} else {
	echo "Номера с + не найдены";
}

echo "<br><br><b><i>Результат поиска в заведенных номерах:</i></b>"."<br>";
if ($log_phone_numbers !== '') {
	echo $log_phone_numbers;
} else {
	echo "Номера с + не найдены";
}


function deletePlusFromPhoneNumbers ($account_id, $auth_token) {
	
	$log = '';
	
	$phone_numbers = Kazoo\get ($account_id, $auth_token, 'phone_numbers');
	
	foreach ($phone_numbers->data->numbers as $number => $value) {
		if (strpos($number, '+') !== false) {
			$log .= 'На ВАТС найден номер '.$number.'<br>';
			$number = str_replace('+', '', $number);
			if (!phone_exists($phone_numbers->data->numbers, $number)) {
				$putPhoneNumber = Kazoo\put ($account_id, $auth_token, 'phone_numbers/'.$number, json_decode('{"data":{}}', false));
				$putActivatePhoneNumber = Kazoo\put ($account_id, $auth_token, 'phone_numbers/'.$number.'/activate', json_decode('{"data":{}}', false));
				$log .= 'Номер '.$number.' загружен на ВАТС и активирован<br>';
			}
			$log .= 'Номер +'.$number.' удален с ВАТС<br>';
			$delPhonenumber = Kazoo\deletePhoneNumber($account_id, $auth_token, '%2B'.$number);
		}
	}
	return $log;
}

function phone_exists ($numbers, $number) {
	
	foreach ($numbers as $key => $value) {
		if($key === $number) return true;
	}
	return false;
	
}


// ##################################################################################

function deletePlusFromCallflows ($account_id, $auth_token) {
	// GET callflows id
	$callflows_id = Kazoo\get($account_id, $auth_token, 'callflows');

	$log = '';

	// Проход по callflows
	foreach ($callflows_id->data as $callflow) {
		$must_update = false;
		// Проход по номерам callflow
		for ($i = 0; $i < count($callflow->numbers); $i++) {
			$plus_pos = strpos($callflow->numbers[$i], '+');
			// Если найден +
			if (strpos($callflow->numbers[$i], '+') !== false) {
				$log .= "В сценарии \"".$callflow->name."\" найден номер ".$callflow->numbers[$i]."<br>";
				$must_update = true;
				// Если номер меньше 12 символов (вместе с +), то удаляем его
				if (strlen($callflow->numbers[$i]) < 12) {
					$log .= $callflow->numbers[$i]." удален из сценария ".$callflow->name."<br>";
					array_splice($callflow->numbers, $i, 1);
					$i--;
				} else {
					// Иначе просто удаляем +
					$callflow->numbers[$i] = str_replace('+', '', $callflow->numbers[$i]);
					$log .= "Символ + удален из номера ".$callflow->numbers[$i]."<br>";
				}
			}
		}
		
		$callflow->numbers = array_values(array_unique($callflow->numbers));

		if ($must_update) {
			$getCallflow = Kazoo\get($account_id, $auth_token, 'callflows/'.$callflow->id);
			$getCallflow->data->numbers = $callflow->numbers;
			
			$postCallflow = Kazoo\post($account_id, $auth_token, 'callflows/'.$callflow->id, $getCallflow);
			if ($postCallflow->status === 'error') {
				$log .= '<br><font color=red>Ошибка! Не удалось сохранить данные.</font><br>Причина:'.$postCallflow->message.'<br>';
			}
			if (isset($postCallflow->data->numbers->unique)) {
				$log .= 'Номер уже привязан к другому сценарию.';
			}
		}
	}
	return $log;
}