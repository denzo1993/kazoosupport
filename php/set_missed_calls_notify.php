<?php

require_once '../kazoo_api.php';

$account_id = $_GET['account_id'];
$auth_token = $_GET['auth_token'];
$user_id = $_GET['user_id'];
$value = $_GET['value'];

if (!(strlen($account_id) === 32 && strlen($auth_token) === 32 && strlen($user_id) === 32 && strlen($value) == 1)) {
	echo '{"error": "Invalid input params"}';
	return;
}

$user = Kazoo\get ($account_id, $auth_token, 'users/'.$user_id);

if ($user->status !== 'success') {
	echo '{"error": "user not found"}';
	return;
}

if ($value == 1) {
	$user->data->missed_calls_notify = true;
} else {
	$user->data->missed_calls_notify = false;
}

$postUser = Kazoo\post ($account_id, $auth_token, 'users/'.$user_id, $user);

echo json_encode ($postUser);