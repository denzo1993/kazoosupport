<?php

class Log {
	
	public static $account_id = '';
	
	public static $L_ERROR = '[ERROR]';
	public static $L_INFO  = '[INFO]';
	
	
	public static function write($msg, $type) {
		$filename = 'log.txt';
		file_put_contents($filename, "  ".$type." [".date('Y-m-d m:h:s')."] [IP:".$_SERVER['REMOTE_ADDR']."] ".Log::$account_id." ".$msg."\n\n", FILE_APPEND);
	}
}