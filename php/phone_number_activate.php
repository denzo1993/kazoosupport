<?php

require_once '../kazoo_api.php';
require_once 'log.php';

$account_id = $_GET['account_id'];
$auth_token = $_GET['auth_token'];
$phone_number = $_GET['phone_number'];

$putActivatePhoneNumber = Kazoo\put ($account_id, $auth_token, 'phone_numbers/'.$phone_number.'/activate', json_decode('{"data":{}}', false));

Log::write("Number activation: ".json_encode($putActivatePhoneNumber), Log::$L_INFO);

echo json_encode($putActivatePhoneNumber);