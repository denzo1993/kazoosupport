<?php

require_once '../kazoo_api.php';
require_once 'log.php';

$account_id = $_GET['account_id'];
$group_id = $_GET['group_id'];
$auth_token = $_GET['auth_token'];

$delGroupRes = Kazoo\deleteGroup($account_id, $auth_token, $group_id);
$callflow_id = Kazoo\getCallflowIDByOwner($account_id, $auth_token, $group_id);
$delCallflowRes = Kazoo\deleteCallflow($account_id, $auth_token, $callflow_id);

Log::write("Delete group: ".json_encode($group_id), Log::$L_INFO);

echo $callflow_id;