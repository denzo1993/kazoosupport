<?php

require_once '../kazoo_api.php';
require_once 'log.php';

$account_id = $_GET['account_id'];
$auth_token = $_GET['auth_token'];

$data = '{"data":{"patterns":{"^2([0-9]{3,15})$":{"module":"transfer","data":{"takeback_dtmf":"*1"}},"^3([0-9]{3,15})$":{"module":"blind_transfer","data":{}}},"numbers":{"0":{"module":"blind_transfer","data":{"target":"801"}},"7":{"module":"hold","data":{"unhold_key":"7"}},"9":{"module":"blind_transfer","data":{"target":"800"}}},"listen_on":"self","binding_digit":"*","digit_timeout_ms":2000}}';

$postMetaflowsRes = Kazoo\postMetaflows($account_id, $auth_token, $data);

echo $postMetaflowsRes;

Log::write("Enable *2 and *3: ".json_encode($postMetaflowsRes), Log::$L_INFO);