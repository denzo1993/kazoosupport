<?php

require_once '../kazoo_api.php';

$account_id = $_GET['account_id'];
$auth_token = $_GET['auth_token'];
$webhook_id = $_GET['webhook_id'];
$value = $_GET['value'];

if (!(strlen($account_id) === 32 && strlen($auth_token) === 32 && strlen($value) == 1)) {
	echo '{"error": "Invalid input params"}';
	return;
}

// disable webhook
if ($value == '0') {
	
	
	if (strlen($webhook_id) !== 32) {
		echo '{"error": "webhook not found"}';
		return;
	}
	$webhook = Kazoo\get ($account_id, $auth_token, 'webhooks/'.$webhook_id);
	if ($webhook->status !== 'success') {
		echo '{"error": "Can not get webhook"}';
		return;
	}
	$webhook->data->enabled = false;
	$postWebhook = Kazoo\post ($account_id, $auth_token, 'webhooks/'.$webhook_id, $webhook);
	echo json_encode($postWebhook);
	
// enable webhook
} else if($value == '1') {
	
	$webhook = Kazoo\get ($account_id, $auth_token, 'webhooks/'.$webhook_id);
	
	// If another webhook already exists
	// Update it
	if (strlen($webhook_id) === 32) {
		$webhook->data->uri = "http://kazoosupport.ztel.ru/support/php/missed_calls_handler.php";
		$webhook->data->http_verb = 'get';
		$webhook->data->retries = 3;
		$webhook->data->enabled = true;
		$postWebhook = Kazoo\post ($account_id, $auth_token, 'webhooks/'.$webhook_id, $webhook);
		echo json_encode($postWebhook);
	} else {
		$webhook = (object) array();
		$webhook->data->name = "New hungup";
		$webhook->data->uri = "http://kazoosupport.ztel.ru/support/php/missed_calls_handler.php";
		$webhook->data->http_verb = "get";
		$webhook->data->hook = "channel_destroy";
		$webhook->data->retries = 3;
		$webhook->data->enabled = true;
		$putWebhook = Kazoo\put ($account_id, $auth_token, 'webhooks/'.$webhook_id, $webhook);
		echo json_encode($putWebhook);
	}
}