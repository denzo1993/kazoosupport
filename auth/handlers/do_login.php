<?php

	require_once '../../php/log.php';

	if($_POST['hash'] == '9872c20d40fb620cad4ed258a33d9b6e') {
		$login = true;
		Log::write("User successfully logged in", Log::$L_INFO);
	} else {
		$login = false;
		Log::write("Invalid credentials", Log::$L_INFO);
	}
	$answer = array(
		"data" => array("login" => $login)
	);
	echo json_encode($answer);
?>