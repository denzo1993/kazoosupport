var keys = [];


function formHandler() {
	var login = document.getElementById("login").value;
	var password = document.getElementById("password").value;
	var hash = login + "|";
	keys.forEach(function(item, i, arr) {
		hash += item;
	});
	hash = md5(login + '|' + password);
	
	$.post( "./handlers/do_login.php", { hash: hash })
		.done(function( data ) {
		console.log( "Data Loaded: " + data );
		var json = JSON.parse(data);
		if (json.data.login == true) {
			var date = new Date(new Date().getTime() + 12 * 60 * 60 * 1000);
			document.cookie = "ksupport=" + hash +"; path=/; expires=" + date.toUTCString();
			var redir = getQueryParams(document.location.search).redir;
			if (redir !== undefined) {
				location.href = '../' + redir;
			} else {
				location.href = '../index.php';
			}
			
		} else {
			alert('Неправильный логин или пароль!');
		}
	});
	
	/*var xhr = new XMLHttpRequest();
	var body = "hash=" + encodeURIComponent(hash);
	xhr.open("POST", '/handlers/do_login.php', true)
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
	xhr.send(body);*/
	
}
	
function onKeyUp(key) {
	// if enter -> go to formHandler()
	if(key == 13) formHandler();
	keys.push(key);
}

function getQueryParams(qs) {
    qs = qs.split('+').join(' ');

    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
}