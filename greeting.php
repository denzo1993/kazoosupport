<?php
	define('account_id', 'e44e61decf894cb4e5e07309cfdc63eb');
	require_once('./kazoo_api.php');
	require_once('functions.php');
	require_once('./auth/handlers/auth.php');
	if (!isAuth()) { header('location: ./auth/login.html'); }
	
	init();
	
	setcookie('account_id', $account_id);
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Zebra ВАТС</title>

    <!-- Bootstrap -->
    <link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <script src="js/greeting.js"></script>
    <script src="js/functions.js"></script>

  </head>
  
  <body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-2">
				<img src="images/logo.png"/>
				<div class="container-fluid">
				<div class="row">
					<form method="GET" onsubmit='return btn_submit();'>
						<div class="col-xs-10">
							<input type="input" autofocus name="domain" id="domain" class="form-control" placeholder="Номер или домен" value="<?php print($_GET['domain'])?>">
						</div>
						<div class="col-xs-2" style="margin-bottom: 30px;">
							<button type="submit" class="btn btn-primary">Go!</button>
						</div>
					</form>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div id='status'></div>
					</div>
				</div>
				</div>
				
				<!-- Menu -->
				<ul class="nav nav-pills nav-stacked">
				  <li><a onclick="goToPage('index.php')">Информация о домене</a></li>
				  <li class="active"><a onclick="goToPage('greeting.php')">Голосовая почта</a></li>
				  <li><a onclick="goToPage('groups.php')">Группы</a></li>
				  <li><a onclick="goToPage('phone_book.php')">Телефонная книга</a></li>
				  <li><a onclick="goToPage('metaflows.php')">Metaflows</a></li>
				  <li><a onclick="goToPage('autocall.php')">Автообзвон</a></li>
				  <li><a onclick="goToPage('first_aid.php')">Скорая помощь</a></li>
				</ul>
			</div>
			<div class="col-xs-10">
				<!-- Content -->
				<?php
					if($_GET['domain'] == '') return;

					if($account_id == '') {
						print('<script>
							document.getElementById("status").innerHTML = "<div class=\'alert alert-danger\' role=\'alert\'><a href=\'#\' class=\'alert-link\'>Домен не найден</a></div>";
						   </script>');
						return;
					};
					
					$vmboxes = Kazoo\getVMBoxes($account_id, $auth_token);
				?>
				
				<div class="row" style="margin-top: 30px;">
					<div class="col-xs-2"></div>
					<div class="col-xs-6">
						<h3>Голосовая почта</h3>
						<table class="table">
						<?php foreach($vmboxes->data as $vmbox) { ?>
							<tr>
								<td class="info">
									<?php
										$vmbox_json = Kazoo\getVMBox($account_id, $vmbox->id, $auth_token);
										echo "<b>".$vmbox->name." (".$vmbox->mailbox.")</b>";
									?>
								</td>
								<td class="info">
									Cообщений: <?php echo $vmbox->messages ?>(100 max)</br>
									<div class="progress">
									  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $vmbox->messages ?>"
									  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $vmbox->messages ?>%">
									    <?php echo $vmbox->messages ?>%
									  </div>
									</div>
									<?php
										if($vmbox_json->data->skip_instructions == true) {

									?>
									<button id='<?php echo $vmbox->id ?>' onclick="set_greetings_false(<?php echo "'".$account_id."','".$vmbox->id."','".$auth_token."'" ?>)" type="button" class="btn btn-warning">Включить стандартное сообщение ГП</button>
									<?php } else { ?>
									<button id='<?php echo $vmbox->id ?>' onclick="set_greetings(<?php echo "'".$account_id."','".$vmbox->id."','".$auth_token."'" ?>)" type="button" class="btn btn-success">Выключить стандартное сообщение ГП</button>
									<?php }	?>
									<br><br><br>
								</td>
							</tr>
						<?php } ?>
						</table>
					</div>
				</div>

				<!-- end Content -->
			</div>
		</div>
	</div>
  </body>
</html>