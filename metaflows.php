﻿<?php
	define('account_id', 'e44e61decf894cb4e5e07309cfdc63eb');
	require_once('./kazoo_api.php');
	require_once('functions.php');
	require_once('./auth/handlers/auth.php');
	if (!isAuth()) { header('location: ./auth/login.html'); }
	
	init();
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Zebra ВАТС</title>

    <!-- Bootstrap -->
    <link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <script src="js/metaflows.js"></script>
    <script src="js/functions.js"></script>

  </head>
  
  <body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-2">
				<img src="images/logo.png"/>
				<div class="container-fluid">
				<div class="row">
					<form method="GET" onsubmit='return btn_submit();'>
						<div class="col-xs-10">
							<input type="input" autofocus name="domain" id="domain" class="form-control" placeholder="Номер или домен" value="<?php print($_GET['domain'])?>">
						</div>
						<div class="col-xs-2" style="margin-bottom: 30px;">
							<button type="submit" class="btn btn-primary">Go!</button>
						</div>
					</form>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div id='status'></div>
					</div>
				</div>
				</div>
				
				<!-- Menu -->
				<ul class="nav nav-pills nav-stacked">
				  <li><a onclick="goToPage('index.php')">Информация о домене</a></li>
				  <li><a onclick="goToPage('greeting.php')">Голосовая почта</a></li>
				  <li><a onclick="goToPage('groups.php')">Группы</a></li>
				  <li><a onclick="goToPage('phone_book.php')">Телефонная книга</a></li>
				  <li class="active"><a onclick="goToPage('metaflows.php')">Metaflows</a></li>
				  <li><a onclick="goToPage('autocall.php')">Автообзвон</a></li>
				  <li><a onclick="goToPage('first_aid.php')">Скорая помощь</a></li>
				</ul>
			</div>
			<div class="col-xs-10">
				<!-- Content -->
				<?php
					if($_GET['domain'] == '') return;

					if($account_id == '') {
						print('<script>
							document.getElementById("status").innerHTML = "<div class=\'alert alert-danger\' role=\'alert\'><a href=\'#\' class=\'alert-link\'>Домен не найден</a></div>";
						   </script>');
						return;
					};
					
					// Check Metaflows
					$metaflowsEnable = Kazoo\getMetaflows($account_id, $auth_token);
					if (isset($metaflowsEnable->name)) {
						if ($metaflowsEnable->name === 'Unauthorized') {
							$auth_token = getNewToken();
							$metaflowsEnable = Kazoo\getMetaflows($account_id, $auth_token);
						}
					}
					
					$transfer_enable = false;
					$blindTransfer_enable = false;
					if(isset($metaflowsEnable->data->patterns))
					foreach($metaflowsEnable->data->patterns as $pattern) {
						if($pattern->module = "transfer") $transfer_enable = true;
						if($pattern->module = "blind_transfer") $blindTransfer_enable = true;
					}
					
					
				?>
				<div class="row" style="margin-top: 50px;">
					<div class="col-xs-2"></div>
					<div class="col-xs-2">
					<?php
						if(!$transfer_enable || !$blindTransfer_enable) {
					?>
						<button id="btn_submit" class="btn btn-info" onclick="postMetaflows('<?php echo $account_id ?>', '<?php echo $auth_token ?>')">Включить трансфер через *2, *3</button>
					<?php } ?>
					</div>
					<div class="col-xs-8">
						<h4><i>
							Включение функции позволяет делать трансфер через *2 и *3</br>
							*2 - обычный перевод ( состояние: <b><?php if($transfer_enable) echo "Включено"; else echo "Выключено"; ?> </b>)</br>
							*3 - слепой перевод  ( состояние: <b><?php if($blindTransfer_enable) echo "Включено"; else echo "Выключено"; ?> </b>)
						</i></h4>
					</div>
				</div>

				<!-- end Content -->
			</div>
		</div>
	</div>
  </body>
</html>