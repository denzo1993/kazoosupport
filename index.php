<?php
require_once('./auth/handlers/auth.php');
if (!isAuth()) {
	header("location: ./auth/login.html?redir=index.php?domain=".$_GET['domain']."");
}

require_once('./kazoo_api.php');
require_once('functions.php');

if (isset($_GET['domain']) & $_GET['domain'] != '') {

    init();

    $phone_numbers = Kazoo\getPhoneNumbers($account_id, $auth_token);
    $registrations = Kazoo\getRegistrations($account_id, $auth_token);

    // Получение id админа
    $getAdmin = Kazoo\getAdmin($account_id, $auth_token);
    $admin_id = null;
    if (isset($getAdmin->data[0]->id)) {
        $admin_id = $getAdmin->data[0]->id;
    }

    // get url
    $url = '#';
    if (strpos($domain->data->realm, '.ztpbx.ru')) {
    	//$auth_token_zt = file_get_contents('/var/www/html/htdocs/kazoosupport/logs/tzt.txt'); // token api.zebratelecom.ru
    	$auth_token_zt = file_get_contents('tzt.txt'); // token api.zebratelecom.ru
        $url = "http://vats.zebratelecom.ru/?account_id=" . $account_id . "&auth_token=" . $auth_token_zt . "&owner_id=" . $admin_id;
    } else {
        $url = "http://vats.gobaza.ru/?account_id=" . $account_id . "&auth_token=" . $auth_token . "&owner_id=" . $admin_id;
    }
}
?>

<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Zebra ВАТС</title>

        <!-- Bootstrap -->
        <link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

        <script src="js/functions.js"></script>
        <script src="js/index.js"></script>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-2">
                    <img src="images/logo.png"/>
                    <div class="container-fluid">
                        <div class="row">
                            <form method="GET" onsubmit='return btn_submit();'>
                                <div class="col-xs-10">
                                    <input type="input" autofocus name="domain" id="domain" class="form-control" placeholder="Номер или домен" value="<?php print($_GET['domain']) ?>">
                                </div>
                                <div class="col-xs-2" style="margin-bottom: 30px;">
                                    <button type="submit" class="btn btn-primary">Go!</button>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div id='status'></div>
                            </div>
                        </div>
                    </div>

                    <!-- Menu -->
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a onclick="goToPage('index.php')">Информация о домене</a></li>
                        <li><a onclick="goToPage('greeting.php')">Голосовая почта</a></li>
                        <li><a onclick="goToPage('groups.php')">Группы</a></li>
                        <li><a onclick="goToPage('phone_book.php')">Телефонная книга</a></li>
                        <li><a onclick="goToPage('metaflows.php')">Metaflows</a></li>
                        <li><a onclick="goToPage('autocall.php')">Автообзвон</a></li>
                        <li><a onclick="goToPage('first_aid.php')">Скорая помощь</a></li>
                        <!--<li><a onclick="goToPage('missed_calls_notify.php')">Уведомление о пропущенных</a></li>!-->
                    </ul>
                </div>
                <div class="col-xs-10">
                    <!-- Content -->
                    <?php
	                    if($_GET['domain'] == '') return;

						if($account_id == '') {
							print('<script>
								document.getElementById("status").innerHTML = "<div class=\'alert alert-danger\' role=\'alert\'><a href=\'#\' class=\'alert-link\'>Домен не найден</a></div>";
							   </script>');
							return;
						};
                    ?>
                    <div class="row" style="margin-top: 30px;">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-6">
                            <h3>Общая информация</h3>
                            <table class="table">

                                <tr>
                                    <td class="info">Домен:</td>
                                    <td class="info">
                                        <a href="<?php echo $url; ?>" target="_blank">
										<?php
											echo $domain->data->realm."<br>";
										?>
                                        </a>
                                        <i>Баланс во всех доменах через данный<br> ресурс некорректный - 8,51 руб</i>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="info">Состояние ВАТС:</td>
                                    <td class="info">
                                        <?php
                                        if ($domain->status == 'error') {
                                            echo 'Ошибка получения данных';
                                        } else {
                                            if ($domain->data->enabled) {
                                                print('<b>Включена </b>');
                                            } else if ($domain->data->enabled == '') {
                                                print('<b>Выключена</b>');
                                            } else {
                                                print('<b>Не определено</b>');
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="info">Используемые номера:</td>
                                    <td class="info">
                                        <?php
                                        
                                        // Get callflows and check number include in someone
                                        $callflows = Kazoo\get ($account_id, $auth_token, 'callflows');
                                        
                                        if (isset($phone_numbers->data->numbers))
                                            foreach ($phone_numbers->data->numbers as $key => $value) {
                                                if ($value->state == 'in_service')
                                                    $number_state = 'Включен';
                                                else
                                                    $number_state = "Выключен <button id=\"btn_number_activate\" class=\"btn btn-success\" onclick=\"phone_number_activate('" . $account_id . "','" . $auth_token . "','" . $key . "')\">Активировать</button>";
                                                print("<b>" . $key . '</b> (' . $number_state . ')');
                                                if (!numberExistInCallflow ($callflows, $key)) {
													print ('<font color=red><i> Номер не назначен схеме</i></font>');
												}
												print ('<br/>');
                                            }
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <h3>Техническая информация</h3>
                            <table class="table">
                                <tr>
                                    <td class="info">
                                        id:
                                    </td>
                                    <td class="info">
<?php print($account_id) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-xs-4">
                            <h3>Активные регистрации</h3>
                            <table class='table'>
                                <?php
                                if (isset($registrations->data) && (is_object($registrations->data) || is_array($registrations->data)))
                                    foreach ($registrations->data as $key => $value) {

                                        $user = Kazoo\getUser($account_id, $value->owner_id, $auth_token);
                                        print("<tr>");

                                        print("<td class='info'>");
                                        print("<b>Sip-логин:</b> " . stringFilter($value->username) . "<br/>");
                                        print("<b>Владелец.:</b> " . stringFilter($user->data->first_name) . "<br/>");
                                        print("<b>Доб.:</b> " . stringFilter($user->data->caller_id->internal->number) . "<br/>");
                                        print("<b>АОН:</b> " . stringFilter($user->data->caller_id->external->number));
                                        print("</td>");

                                        print("<td class='info'>");
                                        print("<b>User agent:</b> " . $value->user_agent . "<br/>");
                                        print("<b>Contact IP:</b> " . $value->contact_ip . "<br/>");
                                        print("<b>Contact port:</b> " . $value->contact_port . "<br/>");
                                        print("<b>Expires:</b> " . $value->expires . "<br/>");
                                        print("</td>");

                                        print("</tr>");
                                    }
                                ?>
                            </table>
                        </div>
                    </div>
                    <!-- end Content -->
                </div>
            </div>
        </div>
    </body>
</html>