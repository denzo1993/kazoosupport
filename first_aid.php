<?php
	require_once('./kazoo_api.php');
	require_once('functions.php');
	require_once('./auth/handlers/auth.php');
	if (!isAuth()) { header('location: ./auth/login.html'); }
	
	init();
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zebra ВАТС</title>

    <!-- Bootstrap -->
    <link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <script src="js/functions.js"></script>
    <script src="js/first_aid.js"></script>

  </head>
  
  <body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-2">
				<img src="images/logo.png"/>
				<div class="container-fluid">
				<div class="row">
					<form method="GET" onsubmit='return btn_submit();'>
						<div class="col-xs-10">
							<input type="input" autofocus name="domain" id="domain" class="form-control" placeholder="Номер или домен" value="<?php print($_GET['domain'])?>">
						</div>
						<div class="col-xs-2" style="margin-bottom: 30px;">
							<button type="submit" class="btn btn-primary">Go!</button>
						</div>
					</form>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div id='status'></div>
					</div>
				</div>
				</div>
				
				<!-- Menu -->
				<ul class="nav nav-pills nav-stacked">
				  <li><a onclick="goToPage('index.php')">Информация о домене</a></li>
				  <li><a onclick="goToPage('greeting.php')">Голосовая почта</a></li>
				  <li><a onclick="goToPage('groups.php')">Группы</a></li>
				  <li><a onclick="goToPage('phone_book.php')">Телефонная книга</a></li>
				  <li><a onclick="goToPage('metaflows.php')">Metaflows</a></li>
				  <li><a onclick="goToPage('autocall.php')">Автообзвон</a></li>
				  <li class="active"><a onclick="goToPage('first_aid.php')">Скорая помощь</a></li>
				</ul>
			</div>
			<div class="col-xs-10">
				
				<?php
					if($_GET['domain'] == '') return;
					
					if($account_id == '') {
						print('<script>
							document.getElementById("status").innerHTML = "<div class=\'alert alert-danger\' role=\'alert\'><a href=\'#\' class=\'alert-link\'>Домен не найден</a></div>";
						   </script>');
						return;
					};

				?>

				<!-- Content -->
				<div class="row" style="margin-top: 50px;">
					<div class="col-xs-2"></div>
					<div class="col-xs-4">
						<div class="checkbox">
						  <label>
						    <input type="checkbox" id="plus_in_numbers">
						    Удаление "+" из заведенных номеров
						  </label>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="alert alert-info" id="infobar">Здесь будет результат выполнения</div>
					</div>
				</div>
				<!-- FIX EXTENSIONS -->
				<div class="row">
					<div class="col-xs-2"></div>
					<div class="col-xs-4">
						<div class="checkbox">
							  <label>
							    <input type="checkbox" id="fix_extensions">
							    Удаление  подвисшего добавочного
							  </label>
						</div>
						<input type="number" id="fix_extensions_number" disabled placeholder="101" min="0" max="999999">
					</div>
					<div class="col-xs-4">
						<div class="alert alert-info" id="infobar_fix_extensions">Здесь будет результат выполнения</div>
					</div>
				</div>
				
				<div class="row" style="margin-top: 20px;">
					<div class="col-xs-2"></div>
					<div class="col-xs-1">
						<button id="btn_aid" class="btn btn-success" onclick="submit(<?php echo "'".$account_id."','".$auth_token."'" ?>)">Вылечить</button>
					</div>
				</div>
				
				<!-- end Content -->
			</div>
		</div>
	</div>
  </body>
</html>