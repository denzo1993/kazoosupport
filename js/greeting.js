﻿var xhr = getXmlHttp();

function set_greetings(account_id, vmbox_id, auth_token) {
	xhr.open('GET', 'php/set_greetings.php?account_id=' + account_id + '&vmbox_id=' + vmbox_id + '&value=1' + '&auth_token=' + auth_token, true);
	xhr.send(null);
}

function set_greetings_false(account_id, vmbox_id, auth_token) {
	xhr.open('GET', 'php/set_greetings.php?account_id=' + account_id + '&vmbox_id=' + vmbox_id + '&value=0' + '&auth_token=' + auth_token, true);
	xhr.send(null);
}

xhr.onreadystatechange = function() {
  if (xhr.readyState == 4) {
     if(xhr.status == 200) {
     	json = JSON.parse(xhr.responseText);
     	elem_id = json.data.id;
     	if(json.data.skip_instructions == true) { btn_text = 'Включить стандартное сообщение ГП'; btn_class = "btn btn-warning"; value = 0; } else { btn_text = 'Выключить стандартное сообщение ГП'; btn_class = "btn btn-success"; value = 1; };
     	document.getElementById(elem_id).innerHTML = btn_text;
     	document.getElementById(elem_id).className = btn_class;
     	document.getElementById(elem_id).setAttribute('value', value);
     	if(json.data.skip_instructions == false) {
     		document.getElementById(elem_id).setAttribute('onclick', "set_greetings('"+getCookie('account_id')+"','"+elem_id+"','"+json.auth_token+"')");
     	} else {
			document.getElementById(elem_id).setAttribute('onclick', "set_greetings_false('"+getCookie('account_id')+"','"+elem_id+"','"+json.auth_token+"')");
		}
     } else {
	 	alert('Ошибка запроса: ' + xhr.status)
	 }
  }
};

function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}

// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}