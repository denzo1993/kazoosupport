
function goToPage (href) {
	var domain = jQuery('#domain').val();
	location.href = href + "?domain=" + domain;
}

function btn_submit() {
		document.getElementById("domain").value = document.getElementById("domain").value.replace(/\s+/g, '');
	
		var input = typeOfInput();

		switch (input) {
			case 'domain zt':
				document.getElementById("status").innerHTML = "<div class=\'alert alert-info\' role=\'alert\'><a href=\'#\' class=\'alert-link\'>Поиск по домену (Зебра)...</a></div>";
				break;
			case 'domain ct':
				document.getElementById("status").innerHTML = "<div class=\'alert alert-info\' role=\'alert\'><a href=\'#\' class=\'alert-link\'>Поиск по домену (ЦТ)...</a></div>";
				break;
			case 'number':
				document.getElementById("status").innerHTML = "<div class=\'alert alert-info\' role=\'alert\'><a href=\'#\' class=\'alert-link\'>Поиск по номеру...</a></div>";
				break;
			case 'domain zt mini':
				document.getElementById("status").innerHTML = "<div class=\'alert alert-info\' role=\'alert\'><a href=\'#\' class=\'alert-link\'>Поиск по домену (Зебра)...</a></div>";
				break;
			case 'account id':
				document.getElementById("status").innerHTML = "<div class=\'alert alert-info\' role=\'alert\'><a href=\'#\' class=\'alert-link\'>Поиск по account id ...</a></div>";
				break;
			default: document.getElementById("status").innerHTML = "<div class=\'alert alert-warning\' role=\'alert\'><a href=\'#\' class=\'alert-link\'>Некорректный домен!</a></div>";
					return false;
		}

		$("#btn_submit").attr('disabled', 'disabled');
		return true;
};
	
function typeOfInput () {
	
	var value = document.getElementById("domain").value;
	
	if (value.indexOf(".ztpbx.ru") >= 5) {
		return 'domain zt';
	} else if (value.indexOf(".vats.gobaza.ru") >= 10) {
		return 'domain ct';
	} else if (value.length == 10 || value.length == 11) {
		return 'number';
	} else if(value.length == 5) {
		document.getElementById("domain").value = value + '.ztpbx.ru';
		return 'domain zt mini';
	} else if(value.length == 32) {
		return 'account id';
	}
	
	return false;
}