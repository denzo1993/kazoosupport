var account_id;
var auth_token;
var user_id;
var value;
var enabled_value;
var xhr = getXmlHttp();

function set_missed_calls_notify (_account_id, _auth_token, _user_id, _value) {
	enableButtons(false);
	
	xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4) {
		 enableButtons(true);
	     if(xhr.status == 200) {
	     	json = JSON.parse(xhr.responseText);
			setBtnStyle(json.data.missed_calls_notify);
	     } else {
		 	alert('Ошибка запроса: ' + xhr.status);
		 }
	  }
	};
	
	account_id = _account_id;
	auth_token = _auth_token;
	user_id = _user_id;
	value = _value;
	xhr.open('GET', './php/set_missed_calls_notify.php?account_id=' + account_id + '&user_id=' + user_id + '&value=' + value + '&auth_token=' + auth_token, true);
	xhr.send(null);
}

function set_missed_calls_notify_enabled (_account_id, _auth_token, _webhook_id, _enabled_value) {
	enableButtons(false);
	
	xhr.onreadystatechange = function() {
	  if (xhr.readyState == 4) {
		 enableButtons(true);
	     if(xhr.status == 200) {
	     	json = JSON.parse(xhr.responseText);
	     	if (json.data.uri == "http://kazoosupport.ztel.ru/support/php/missed_calls_handler.php" && json.data.enabled == true) {
				setEnableBtnStyle(true);
			} else {
				setEnableBtnStyle(false);
			}
	     } else {
		 	alert('Ошибка запроса: ' + xhr.status);
		 }
	  }
	};
	
	account_id = _account_id;
	auth_token = _auth_token;
	enabled_value = _enabled_value;
	webhook_id = _webhook_id;
	xhr.open('GET', './php/set_webhook.php?account_id=' + account_id + '&webhook_id=' + webhook_id + '&value=' + enabled_value + '&auth_token=' + auth_token, true);
	xhr.send(null);
}

function setEnableBtnStyle (value) {
	var btn = document.getElementById('notify_enabled');
	if (value) {
		btn.className = 'btn btn-success';
		btn.innerHTML = 'Выключить оповещения в домене';
		enabled_value = 0;
	} else {
		btn.className = 'btn btn-info';
		btn.innerHTML = 'Включить оповещения в домене';
		enabled_value = 1;
	}
	btn.setAttribute('onclick', "set_missed_calls_notify_enabled('" + account_id + "','" + auth_token + "','" + webhook_id + "','" + enabled_value + "')");
}

function setBtnStyle (value) {
	var btn = document.getElementById('btn_notify_' + json.data.id);
	if (value) {
		btn.className = 'btn btn-success';
		btn.innerHTML = 'Выключить оповещения';
		value = 0;
	} else {
		btn.className = 'btn btn-info';
		btn.innerHTML = 'Включить оповещения';
		value = 1;
	}
	btn.setAttribute('onclick', "set_missed_calls_notify('" + account_id + "','" + auth_token + "','" + user_id + "','" + value + "')");
}

function enableButtons (value) {
	var btns = document.getElementsByTagName('button');
	if (!value) {
		$.each (btns, function (key, btn) {
			btn.setAttribute ('disabled', 'disabled');
		});
	} else {
		$.each (btns, function (key, btn) {
			btn.removeAttribute ('disabled');
		});
	}
}

function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}