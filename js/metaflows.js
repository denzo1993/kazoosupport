var xhr = getXmlHttp();

function postMetaflows(account_id, auth_token, group_id) {
	xhr.open('GET', './php/postMetaflows.php?account_id='+account_id+'&auth_token='+auth_token);
	document.getElementById('btn_submit').innerHTML = 'Выполняется...';
	document.getElementById('btn_submit').setAttribute('disabled', 'disabled');
	xhr.send(null);
}

xhr.onreadystatechange = function() {
	if(xhr.readyState == 4) {
		if(xhr.status == 200) {
			document.getElementById('btn_submit').innerHTML = 'Сделано';
						alert('Запрос выполненен!');
		} else {
			alert('Ошибка запроса');
		}
	}
}

function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}