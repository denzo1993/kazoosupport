﻿var xhr = getXmlHttp();

function deleteGroup(account_id, auth_token, group_id) {
	xhr.open('DELETE', './php/delete_group.php?account_id='+account_id+'&auth_token='+auth_token+'&group_id='+group_id);
	xhr.send(null);
}

xhr.onreadystatechange = function() {
	if(xhr.readyState == 4) {
		if(xhr.status == 200) {
			alert('Группа удалена');
			location.reload();
		} else {
			alert('Ошибка запроса');
		}
	}
}

function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}