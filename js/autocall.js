var xhr = getXmlHttp();

function add_autocall(account_id, auth_token) {
	xhr.open('GET', './php/add_autocall.php?account_id='+account_id+'&auth_token='+auth_token);
	document.getElementById('btn_submit').innerHTML = 'Выполняется...';
	document.getElementById('btn_submit').setAttribute('disabled', 'disabled');
	xhr.send(null);
}

xhr.onreadystatechange = function() {
	if(xhr.readyState == 4) {
		if(xhr.status == 200) {
			document.getElementById('btn_submit').innerHTML = 'Сделано';
						alert('Запрос выполнен!');
		} else {
			alert('Ошибка запроса');
		}
	}
}

function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}