var xhr = getXmlHttp();

function phone_number_activate(account_id, auth_token, phone_number) {
	document.getElementById('btn_number_activate').setAttribute('disabled', 'disabled');
	xhr.open('GET', './php/phone_number_activate.php?account_id=' + account_id + '&auth_token=' + auth_token + '&phone_number=' + phone_number, true);
	xhr.send(null);
}

xhr.onreadystatechange = function() {
  if (xhr.readyState == 4) {
     if(xhr.status == 200) {
     	json = JSON.parse(xhr.responseText);
     	if (json.status == 'success') {
			alert('Номер активирован!\nСтраница будет обновлена');
			location.reload();
		} else {
			alert('Ошибка в активации номера! Попробуйте чуть позже');
			document.getElementById('btn_number_activate').removeAttribute('disabled');
		}
     	console.log(xhr.responseText);
     } else {
	 	alert('Ошибка запроса: ' + xhr.status)
	 }
  }
};

function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}