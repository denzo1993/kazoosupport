function submit(account_id, auth_token) {

	if (!confirm('Действительно выполнить операцию?')) {
		return;
	}

	var do_operation = false;

	// Отправляем запрос на удаление +, если выбран чек бокс
	if (jQuery('#plus_in_numbers').prop('checked')) {
		do_operation = true;
		document.getElementById('infobar').innerHTML = 'Выполняется...';
		delete_plus(account_id, auth_token);
	}
	
	// Отправляем запрос на чистку добавочных, если выбран чек бокс
	if (jQuery('#fix_extensions').prop('checked')) {
		do_operation = true;
		document.getElementById('infobar_fix_extensions').innerHTML = 'Выполняется...';
		fix_extensions(account_id, auth_token);
	}
	
	// Если хотя бы одна операция выполяется, то информируем об этом
	if(do_operation) {
		document.getElementById('btn_aid').setAttribute('disabled', 'disabled');
	} else {
		alert('Ничего не выбрано');
	}
	
}

var xhr = getXmlHttp();
var xhr_fix_extension = getXmlHttp();

function delete_plus (account_id, auth_token) {
	xhr.open('GET', './php/delete_plus.php?account_id=' + account_id + '&auth_token=' + auth_token, true);
	xhr.send(null);
}

xhr.onreadystatechange = function() {
  if (xhr.readyState == 4) {
  	document.getElementById('btn_aid').removeAttribute('disabled');
     if(xhr.status == 200) {
     	document.getElementById('infobar').innerHTML = xhr.responseText;
     } else {
	 	alert('Ошибка запроса delete_plus: ' + xhr.status)
	 }
  }
};

// FIX EXTENSIONS
function fix_extensions (account_id, auth_token) {
	var extension = document.getElementById('fix_extensions_number').value;
	xhr_fix_extension.open('GET', './php/fix_extensions.php?account_id=' + account_id + '&auth_token=' + auth_token + '&extension=' + extension, true);
	xhr_fix_extension.send(null);
}

xhr_fix_extension.onreadystatechange = function() {
  if (xhr_fix_extension.readyState == 4) {
  	document.getElementById('btn_aid').removeAttribute('disabled');
     if(xhr_fix_extension.status == 200) {
     	var json = JSON.parse(xhr_fix_extension.responseText);
     	document.getElementById('infobar_fix_extensions').innerHTML = json.text;
     } else {
	 	alert('Ошибка запроса fix_extensions: ' + xhr_fix_extension.status)
	 }
  }
};

function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}

$(document).ready(function () {
	$('#fix_extensions').change( function () {
		if ($(this).is(':checked')) {
			$('#fix_extensions_number').prop('disabled', false);
		} else {
			$('#fix_extensions_number').prop('disabled', true);
		}
	});
});