<?php
	// Возвращает true, если включен keep_caller_id. Иначе false
	function get_keep_caller_id($device_json) {
		if($device_json['data']['call_forward']['keep_caller_id'] == 1) return true;
		return false;
	}
	
	// Устанавливает keep_caller_id
	function set_keep_caller_id($device_json, $value) {
		print($value."<br/>");
		if($value == 'true') $value = true; else $value = false;
		$device_json->data->call_forward->keep_caller_id = $value;
		return $device_json;
	}
	
	// Возвращает true, если device = cellphone	
	function isCellphone($device_json_preview) {
		if($device_json_preview['device_type'] == 'cellphone') return true;
		return false;
	}
	
	function stringFilter($value) {
		if($value == '') return 'Не задано';
		return $value;
	}
	
	// Возвращает ID registry
	function getRegistryID($lists_json) {
		foreach($lists_json->data as $key => $value) {
			if($value->name == 'registry') return $value->id;
		}
		return '';
	}
	
	// Возвращает firstNumber
	function getFirstNumber($registry_json) {
		foreach($registry_json->data->entries as $key => $value) {
			if($value->pattern == 'firstNumber') return $value->firstname;
		}
		return '';
	}
	
	// Оставляет в массиве только cellphone
	function getCellphones($devices) {
		if(!isset($devices)) return null;
		foreach($devices as $key => $value) {
			if($value['device_type'] == 'cellphone') $res[] = $value;
		}
		return $res;
	}
	
	function checkDomain($domain) {
		if(strpos($domain, ".ztpbx.ru") < 3) return false;
		return true;
	}
	
	function auth () {
		$filename = '/var/www/html/htdocs/kazoosupport/logs/t.txt';
		$filename = 't.txt';
		$fromFile = file_get_contents($filename);
		
		if (strlen($fromFile) !== 32) {
			$auth_token = getNewToken($filename);
			return $auth_token;
		} else {
			return $fromFile;
		}
	}
	
	function getNewToken () {
		$filename = '/var/www/html/htdocs/kazoosupport/logs/t.txt';
		$filename = 't.txt';
		$auth_token = Kazoo\getToken();
		file_put_contents($filename, $auth_token);
		return $auth_token;
	}
	
	function getAccountIDFromFile ($domain) {
		
		$filename = '/var/www/html/htdocs/kazoosupport/logs/accounts.txt';
		$filename = 'accounts.txt';
		$fromFile = file_get_contents($filename);
		$accounts = json_decode($fromFile, false);
		
		foreach ($accounts->data as $account) {
			if ($account->name === $domain) {
				return $account->id;
			}
		}
		
		return null;
	}
	
	function init () {
		if($_GET['domain'] != '') {
		    // Авторизация
	    global $auth_token;
		global $account_id;
		global $domain;
		
		$auth_token = auth();
		
		if (strlen($_GET['domain']) == 32) {
			// if input param == account_id
			$account_id = $_GET['domain'];
		} else {
			// Получаем ID домена
		    if (strpos($_GET['domain'], '.ztpbx.ru') || strpos($_GET['domain'], '.vats.gobaza.ru')) {
		        $account_id = Kazoo\getDomainID($auth_token, $_GET['domain']);
		    } else {
		        $account_id = Kazoo\getDomainIDByNumber($auth_token, $_GET['domain']);
		    }

		    if (strlen($account_id) !== 32) {
		        $auth_token = getNewToken();
		        // Получаем ID домена со свежим токеном
		        if (strpos($_GET['domain'], '.ztpbx.ru') || strpos($_GET['domain'], '.vats.gobaza.ru')) {
		            $account_id = Kazoo\getDomainID($auth_token, $_GET['domain']);
		        } else {
		            $account_id = Kazoo\getDomainIDByNumber($auth_token, $_GET['domain']);
		        }
		    }
		}
	    
	    $account_id = str_replace('	', '', str_replace(' ', '', $account_id));

	    $domain = Kazoo\getDomain($account_id, $auth_token);
		}
	}
	
	function numberExistInCallflow ($callflows, $number) {
		
		foreach ($callflows->data as $callflow) {
			if (in_array($number, $callflow->numbers)) {
				return true;
			}
		}
		
		return false;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	